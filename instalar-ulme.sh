#!/bin/bash
COR='\033[0;36m'
NC='\033[0m' # No Color

if [[ ! $(wine --version | grep '\-5.') ]]; then
    printf "${COR}Foi detectado que sua versão do wine não é a mais atual, deseja instalar uma nova versão?${NC}\n"
    read -p "[S]im ou [N]ão? >>> " -n 1 -r
    if [[ $REPLY =~ ^[Ss]$ ]]; then
        echo -e " \n"
        printf "${COR}Vamos tentar instalar a versão mais nova do WINE.${NC}\n"
        wget https://download.opensuse.org/repositories/Emulators:/Wine:/Debian/xUbuntu_18.04/Release.key
        sudo apt-key add Release.key

        apt add-repository 'deb https://download.opensuse.org/repositories/Emulators:/Wine:/Debian/xUbuntu_18.04/ ./'
        apt update
        apt install libfaudio0 
        apt install --install-recommends winehq-devel
    fi
fi

# baixa a última versão atualizada do winetricks, extrai na pasta atual e dá 
# permissão de execução.
printf "${COR}Baixando uma versão atualizada do winetricks. ${NC}\n"
wget https://github.com/Winetricks/winetricks/archive/20200412.zip
unzip -j 20200412.zip  winetricks-20200412/src/winetricks -d . 
chmod + x winetricks

# Se o instalador do programa não está na pasta, vai fazer download
[[ ! -f "ulme.exe" ]] && wget "http://ulme.com.br/ulme.exe"

# Criando o prefixo do wine onde o programa será instalado
printf "${COR}Criando o prefixo do wine onde o programa será instalado. ${NC}\n"
WINEPREFIX=~/.ulme WINEARCH=win32 wineboot -u

# às vezes essa pasta fica com proteção contra escrita, isso não deixa o 
# wmp9 instalar
chmod -R +w "~/.ulme/dosdevices/c:/windows/system32/"

# instalando o windows media player 9
printf "${COR}Tentando instalar o windows media player 9. ${NC}\n"
WINEPREFIX=~/.ulme WINEARCH=win32 ./winetricks wmp9

# instalando o programa
printf "${COR}Instalando o uLme. ${NC}\n"
WINEPREFIX=~/.ulme WINEARCH=win32 wine "ulme.exe"
